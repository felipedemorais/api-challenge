from django.conf.urls import include, url
from rest_framework import routers
from galeria import views

router = routers.DefaultRouter()
router.register(r'api/albums', views.AlbumAllViewSet)
# router.register(r'api/albums/(?P<id_album>d)', views.AlbumSpecificViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
