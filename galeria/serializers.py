# -*- coding: utf-8 -*-

from galeria.models import Album, Photo
from rest_framework import serializers


class AlbumSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Album
        fields = ('id', 'name', 'description')

# TODO
# 3 ÚLTIMAS FOTOS


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        
        model = Photo
        fields = ('id', 'album', 'image', 'date',  'description')