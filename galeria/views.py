from galeria.models import Album, Photo
from rest_framework import viewsets
from galeria.serializers import AlbumSerializer, PhotoSerializer

class AlbumAllViewSet(viewsets.ModelViewSet):
    
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer

# class AlbumSpecificViewSet(viewsets.ModelViewSet):
    
#     queryset = Album.objects.filter(id=)
#     serializer_class = AlbumSerializer
#     import ipdb; ipdb.set_trace()
#     pass


# # TODO 
# # Photo