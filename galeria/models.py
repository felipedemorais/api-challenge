from django.db import models


class Album(models.Model):

    class Meta:
        verbose_name = "Album"
        verbose_name_plural = "Albuns"

    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Photo(models.Model):

    class Meta:
        verbose_name = "Photo"
        verbose_name_plural = "Photos"

    album = models.ForeignKey('Album')
    image = models.ImageField(upload_to='../images/')
    date = models.DateTimeField()
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.description
